package com.tema3.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

/**
 * Created by Sergio on 21/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class MyFragment extends androidx.fragment.app.Fragment {

    private TextView frTv;
    private ConstraintLayout frCl;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, container, false);
        frTv = view.findViewById(R.id.fr_textView);
        frCl = view.findViewById(R.id.fr_layout);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //el método getArguments permite recoger datos pasados al fragment por bundle
        int position = getArguments().getInt("position");

        frTv.setText("Fragment " + (position + 1));

        int color;
        switch (position) {
            case 0:
                color = Color.MAGENTA;
                break;

            case 1:
                color = Color.CYAN;
                break;

            case 2:
                color = Color.GREEN;
                break;

            default:
                color = Color.LTGRAY;
                break;

        }
        frCl.setBackgroundColor(color);
    }
}
