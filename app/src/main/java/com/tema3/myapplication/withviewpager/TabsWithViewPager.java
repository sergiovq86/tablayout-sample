package com.tema3.myapplication.withviewpager;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.tema3.myapplication.R;

import java.util.ArrayList;

/**
 * Created by Sergio on 21/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class TabsWithViewPager extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs_with_pager);

        //configuracion toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Mi Biblioteca");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        //arraylist para el adaptador
        ArrayList<Libro> listaLibros = new ArrayList<>();
        listaLibros.add(new Libro(getString(R.string.libro1_title), getString(R.string.libro1_author),
                getString(R.string.libro1_sinopsis), getString(R.string.libro1_year), R.drawable.lordofrings));
        listaLibros.add(new Libro(getString(R.string.libro2_title), getString(R.string.libro2_author),
                getString(R.string.libro2_sinopsis), getString(R.string.libro2_year), R.drawable.davinci));
        listaLibros.add(new Libro(getString(R.string.libro3_title), getString(R.string.libro3_author),
                getString(R.string.libro3_sinopsis), getString(R.string.libro3_year), R.drawable.juegotronos));
//        listaLibros.add(new Libro(getString(R.string.libro4_title), getString(R.string.libro4_author),
//                getString(R.string.libro4_sinopsis), getString(R.string.libro4_year), R.drawable.annefrank));
//        listaLibros.add(new Libro(getString(R.string.libro5_title), getString(R.string.libro5_author),
//                getString(R.string.libro5_sinopsis), getString(R.string.libro5_year), R.drawable.harrypotter));

        //constructor de mi adaptador
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), getLifecycle(), listaLibros);
        ViewPager2 viewPager = findViewById(R.id.viewpager);
        //se asigna adaptador al viewpager
        viewPager.setAdapter(adapter);

        //definicion tablayout
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        //TAbLayoutMediator permite sincronizar un tablayout con un viewpager
        TabLayoutMediator mediator = new TabLayoutMediator(tabLayout, viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                //método para configurar los tabs
                tab.setText("Libro " + (position + 1));
            }
        });
        //sincroniza el Mediator
        mediator.attach();

//        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //codigo para la pulsacion del botón atrás
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
