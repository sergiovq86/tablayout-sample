package com.tema3.myapplication.withviewpager;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sergio on 21/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class Libro implements Parcelable {

    private String title, author, sinopsis, year;
    private int photo;

    public Libro(String title, String author, String sinopsis, String year, int photo) {
        this.title = title;
        this.author = author;
        this.sinopsis = sinopsis;
        this.year = year;
        this.photo = photo;
    }

    protected Libro(Parcel in) {
        title = in.readString();
        author = in.readString();
        sinopsis = in.readString();
        year = in.readString();
        photo = in.readInt();
    }

    public static final Creator<Libro> CREATOR = new Creator<Libro>() {
        @Override
        public Libro createFromParcel(Parcel in) {
            return new Libro(in);
        }

        @Override
        public Libro[] newArray(int size) {
            return new Libro[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(sinopsis);
        dest.writeString(year);
        dest.writeInt(photo);
    }
}
