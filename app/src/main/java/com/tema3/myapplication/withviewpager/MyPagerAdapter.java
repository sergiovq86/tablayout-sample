package com.tema3.myapplication.withviewpager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;

/**
 * Created by Sergio on 21/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class MyPagerAdapter extends FragmentStateAdapter {

    private final ArrayList<Libro> listaLibros;

    //constructor del adaptador. FragmentStateAdapter necesita de forma obligatoria implementar uno de los tres constructores que ofrece:
    // - Uno que recibe el FragmentManager y el lifecycle
    // - Uno que recibe un Fragment, por si el viewpager está en un adapter
    // - Uno que recibe un FragmentActivity
    // Hemos utilizado la primera opción y además hemos añadido el ArrayList que vamos a usar en el viewpager
    public MyPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, ArrayList<Libro> listaLibros) {
        super(fragmentManager, lifecycle);
        this.listaLibros = listaLibros;
    }

    @NonNull
    @Override
    //Método para crear los fragments del viewpager. En este caso, el fragment es el mismo para todas las páginas,
    //solamente cambia el contenido del fragment.
    //si se usaran distintos fragment, habría que crear cada uno de ellos según la posición del viewpager en la que van a aparecer
    public Fragment createFragment(int position) {
        Libro libro = listaLibros.get(position);
        FragmentLibro fragment = FragmentLibro.newInstance(libro, position);
        return fragment;
    }

    @Override
    //método que devuelve el número de página del viewpager. En nuestro caso es el tamaño del arraylist
    public int getItemCount() {
        return listaLibros.size();
    }
}
