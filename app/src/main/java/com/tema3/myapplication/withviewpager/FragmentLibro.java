package com.tema3.myapplication.withviewpager;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.tema3.myapplication.R;

/**
 * Created by Sergio on 21/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class FragmentLibro extends Fragment {
    //variable estática usada para pasar datos
    private static final String ARG_BOOK = "book";
    private static final String ARG_POS = "position";
    private TextView tvTitle, tvAuthor, tvYear, tvSinopsis;
    private ImageView ivPhoto;
    private ConstraintLayout frCl;

    private Libro libro;
    private int position;

    /**
     * Método para crear una instancia de FragmentLibro
     * @param book -> Recibe un parámetro de tipo Libro que será usado dentro del Fragment
     * @param position -> Posición que ocupa el fragment en el viewpager
     * @return -> Devuelve un Fragment ya creado
     */
    public static FragmentLibro newInstance(Libro book, int position) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_BOOK, book);
        args.putInt(ARG_POS, position);

        FragmentLibro fl = new FragmentLibro();
        fl.setArguments(args);
        return fl;
    }

    @Override
    //onCreate es el mejor método para recibir los parámetros del Bundle
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null){
            if (arguments.containsKey(ARG_BOOK)) {
                libro = arguments.getParcelable(ARG_BOOK);
            }
            if (arguments.containsKey(ARG_POS)) {
                position = arguments.getInt(ARG_POS);
            }
        }
    }

    @Nullable
    @Override
    // se inicializa la vista del Fragment
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book, container, false);
        tvAuthor = view.findViewById(R.id.tv_author);
        tvSinopsis = view.findViewById(R.id.tv_sinopsis);
        tvTitle = view.findViewById(R.id.tv_title);
        tvYear = view.findViewById(R.id.tv_year);
        ivPhoto = view.findViewById(R.id.iv_photo);
        frCl = view.findViewById(R.id.cl_layout);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //en la variable libro tendremos los datos del libro
        tvAuthor.setText(libro.getTitle());
        tvYear.setText(libro.getYear());
        tvTitle.setText(libro.getTitle());
        tvSinopsis.setText(libro.getSinopsis());
        ivPhoto.setImageResource(libro.getPhoto());

        int color;
        //la variable position guardará la posición del fragment dentro del viewpager
        switch (position) {
            case 0:
                color = getContext().getResources().getColor(R.color.color1);
                break;

            case 1:
                color = getContext().getResources().getColor(R.color.color2);
                break;

            case 2:
                color = getContext().getResources().getColor(R.color.color3);
                break;

            case 3:
                color = getContext().getResources().getColor(R.color.color4);
                break;

            case 4:
                color = getContext().getResources().getColor(R.color.color5);
                break;

            default:
                color = Color.LTGRAY;
                break;

        }
        frCl.setBackgroundColor(color);
    }
}
