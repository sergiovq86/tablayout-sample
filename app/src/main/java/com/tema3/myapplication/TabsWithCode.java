package com.tema3.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;

/**
 * Created by Sergio on 21/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class TabsWithCode extends AppCompatActivity {

    private TabLayout tabLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs_with_code);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Frag 1").setIcon(R.drawable.ic_baseline_flight_takeoff_24));
        tabLayout.addTab(tabLayout.newTab().setText("Frag 2"));
        tabLayout.addTab(tabLayout.newTab().setText("Frag 3").setIcon(R.drawable.ic_baseline_thumb_up_off_alt_24));

        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        TabLayout.Tab tab2 = tabLayout.getTabAt(1);
        if (tab2 != null) {
            tab2.setIcon(R.drawable.ic_baseline_time_to_leave_24);
            tabLayout.selectTab(tab2);
            navigateToFragment(1);
        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                navigateToFragment(id);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                Log.d("TABS", "Tab " + (id + 1) + " desmarcado");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                Toast.makeText(TabsWithCode.this, "Tab " + (id + 1) + " reelegido", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void navigateToFragment(int itemId) {
        // getString es un método de la clase Activity para obtener textos de res/values/strings.xml
        TabLayout.Tab tabPulsado = tabLayout.getTabAt(itemId);
        String title = String.valueOf(tabPulsado.getText());

        Fragment fragment = new MyFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", itemId);
        fragment.setArguments(bundle);

        // según la id del item pulsado, creamos ese fragment, y cambiamos el titulo
//        switch (itemId) {
//            default:
//                fragment = new Tab1Fragment();
//                break;
//
//            case 1:
//                fragment = new Tab2Fragment();
//                break;
//
//            case 2:
//                fragment = new Tab3Fragment();
//                break;
//        }

        // transacción del fragment al framelayout
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.frameLayout, fragment);
        transaction.commit();

        // asignamos título al toolbar
        setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
