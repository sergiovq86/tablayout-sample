package com.tema3.myapplication;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.tabs.TabLayout;

/**
 * Created by Sergio on 21/03/2021.
 * Copyright (c) 2021 Qastusoft. All rights reserved.
 */

public class TabsWithXml extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabs_with_xml);

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                navigateToFragment(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                Log.d("TABS", "Tab " + (id + 1) + " desmarcado");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int id = tab.getPosition();
                Toast.makeText(TabsWithXml.this, "Tab " + (id + 1) + " reelegido", Toast.LENGTH_SHORT).show();
            }
        });

        navigateToFragment(0);


        //tabLayout.selectTab(tabLayout.getTabAt(1));
    }

    private void navigateToFragment(int id) {
        //para pasar datos a un fragment, se crea un bundle y se añade el dato bajo una clave
        //despues el bundle se añade al fragment con el método setArguments
        Bundle bundle = new Bundle();
        bundle.putInt("position", id);
        Fragment fragment = new MyFragment();
        fragment.setArguments(bundle);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.framelayout, fragment);
        transaction.commit();
    }
}
